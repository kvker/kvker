---
layout: note
title: add柯里化实例
excerpt: 可做参考拓展别的柯里化
date: 2022-6-6 11:18:43
updated: 2022-6-6 11:18:43
comments: false
lang: zh-CN
---

```js
class Curry {
    constructor() {
        this.array = []
    }

    add() {
        if(arguments.length) {
            console.log(arguments)
            this.array = [...this.array, arguments[0]]
            return this.add.bind(this)
        } else {
            return this.array.reduce((p, c) => p += c)
        }
    }
}
let curry = new Curry()
curry.add(1)(2)(3)() // return 6
```
---
layout: note
title: termux安卓上无法访问文件夹
excerpt: 本质是权限未开
date: 2022-5-29 14:17:11
updated: 2022-5-29 14:17:11
comments: false
lang: zh-CN
---

在命令行中调用`termux-setup-storage`即可现实的调用获取权限弹框，同意即可。
并且会同步生成对应文件夹下的映射文件夹。
---
layout: note
title: 七牛云鉴权AccessKey生成Nodejs
excerpt: 一些坑点提示
date: 2022-7-17 22:34:17
updated: 2022-7-17 22:34:17
comments: false
lang: zh-CN
---

有个坑，所谓URL安全的Base64，其实就是对base64进行URL相关的非法字符替换，具体为`safe_base64 = base64.replace(/\//g, '_').replace(/\+/g, '-')`

api.js
```js
const express = require('express')
  , router = express.Router()
  , axios = require('axios')
  , { getQiniuAuthorization } = require('../util/qiniu')

// {"method": "POST", "host": "ai.qiniuapi.com", "path": "/v3/image/censor", "content_type": "application/json", "extra_data": {"data": {"uri": "https://mars-assets.qnssl.com/resource/gogopher.jpg"}, "params": {"scenes": ["pulp", "terror", "politician",  "ads"]}}}
router.post('/check/image', (req, res, next) => {
  const data = { data: req.body, "params": { "scenes": ["pulp", "terror", "politician", "ads"] } }
    , check_image_params = { "method": "POST", "host": "ai.qiniuapi.com", "path": "/v3/image/censor", "content_type": "application/json", data, query: req.query }

  getQiniuAuthorization(check_image_params).then(ret => {
    console.log(ret.authorization)
    return axios.post('http://ai.qiniuapi.com/v3/image/censor', data, {
      headers: {
        'Content-Type': "application/json",
        Authorization: ret.authorization,
      },
    })
  }).then(({ data }) => {
    if (data.result.suggestion === 'pass') {
      res.json(successHandler(data))
    } else {
      res.json(failHandler(400, '图片违规'))
    }
  }).catch(error => {
    const data = error.response.data
    res.json(failHandler(data.code, data.message))
  })
})

module.exports = router
```

util.js
```js
const CryptoJS = require('crypto-js')

if (!global.qiniu) {
  global.qiniu = {
    AK: 'AK',
    SK: 'SK',
  }
}

module.exports = {
  /**
   * 获取七牛的authorization
   */
  getQiniuAuthorization(params) {
    const { host, path, method, content_type, data, query } = params
      , row_query = new URLSearchParams(query).toString()
    let flag = `${method} ${path}${row_query ? '?' + row_query : ''}\nHost: ${host}\nContent-Type: ${content_type}\n\n${data ? JSON.stringify(data) : ''}`
    // console.log(flag)
    const base64 = CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA1(flag, qiniu.SK)).replace(/\//g, '_').replace(/\+/g, '-')
      , authorization = `Qiniu ${qiniu.AK}:${base64}`
    // console.log({ authorization })
    return Promise.resolve({ authorization })
  },
}
```
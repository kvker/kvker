---
layout: note
title: CSS的background-size设置
excerpt: 不单单只有具体的尺寸和比例
date: 2022-7-26 9:36:15
updated: 2022-7-26 9:36:15
comments: false
lang: zh-CN
---

|值|描述|
|-|-|
|length|`宽 高`，未填写的默认为"auto"。|
|percentage|父元素的百分比`宽 高`，未填写的默认为"auto"。|
|cover|短边充满，参考object-fit。|
|contain|长边充满，参考object-fit。|
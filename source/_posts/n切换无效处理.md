---
layout: note
title: n切换无效处理
excerpt: Read more...
date: 2023-2-28 10:5:46
updated: 2023-2-28 10:5:46
comments: false
lang: zh-CN
---

~/.bash_profile
```bash
export N_PREFIX=/usr/local
export PATH=$N_PREFIX/bin:$PATH
```
---
layout: note
title: CR-CRLF-LF区别
excerpt: 文本换行的差异，不同system有不同问题。
date: 2022-6-14 0:22:9
updated: 2022-6-14 0:22:9
comments: false
lang: zh-CN
---

* CR：Carriage Return，对应ASCII中转义字符\r，表示回车。MacIntosh操作系统（即早期的Mac操作系统）采用单个字符CR来进行换行。
* CRLF：Carriage Return & Linefeed，\r\n，表示回车并换行。Windows操作系统采用两个字符来进行换行，即CRLF。
* LF：Linefeed，对应ASCII中转义字符\n，表示换行。Unix/Linux/Mac OS X采用LF。

由于部分Linux只支持LF，所以建议把编辑器或IDE均设置为LF。顺便可以解决格式化时候出现`^M`问题。
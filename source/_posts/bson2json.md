---
layout: note
title: bson2json
excerpt: Read more...
date: 2023-8-22 17:37:31
updated: 2023-8-22 17:37:31
comments: false
lang: zh-CN
---

```js
const fs = require('fs')
const BSON = require('bson')

function BSON2JSON (from) {
  const buffer = fs.readFileSync(from)
  let index = 0
  const documents = []
  while (buffer.length > index) {
    index = BSON.deserializeStream(buffer, index, 1, documents, documents.length)
  }

  return documents
}

const bsonFilePath = 'KSAppTabUserMap.bson'
const bson2json = BSON2JSON(bsonFilePath)
console.log('🚀 ~ file: bson2json:', bson2json)
fs.writeFileSync('KSAppTabUserMap.json', JSON.stringify(bson2json))
```
---
layout: note
title: this隐式具有类型 any因为它没有类型注释
excerpt: Read more...
date: 2022-9-13 16:2:30
updated: 2022-9-13 16:2:30
comments: false
lang: zh-CN
---

```js
Function.prototype.before = function (fn) {
  const self = this
  return function () {
    fn.apply(this, arguments) // 本行报错
    return self.apply(this, arguments)
  }
}
```

修改为

```js
Function.prototype.before = function (fn) {
  const self = this
  return function (this: Function) {
    fn.apply(this, arguments)
    return self.apply(this, arguments)
  }
}
```
---
layout: note
title: windows屏幕缩放页面适配处理
excerpt: Read more...
date: 2022-7-6 15:54:7
updated: 2022-7-6 15:54:7
comments: false
lang: zh-CN
---

```js
window.addEventListener('resize', () => {
    let e = window.screen.width || 1920
    let zm = (e === 1366 ? 0.8 : e / 1680)
    let radio = Math.min(Number(zm.toFixed(2)), 1)
    document.documentElement.style.zoom = radio
}, false)
```
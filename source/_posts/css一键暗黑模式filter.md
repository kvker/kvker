---
layout: note
title: css一键暗黑模式filter
excerpt: Read more...
date: 2022-11-21 9:27:0
updated: 2022-11-21 9:27:0
comments: false
lang: zh-CN
---

注意background需要显式的配置颜色。
```css
html {
    filter: invert(100%) hue-rotate(180deg);
    background: #ffffff;
}
```
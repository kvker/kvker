---
layout: note
title: Gitee编辑Hexo的文章生成title
excerpt: 使用Gitee与Hexo自动化更新Blog文章时候，在Gitee编辑文章生成Hexo顶部标记位。
date: 2022-6-2 9:14:4
updated: 2022-6-2 9:14:4
comments: false
lang: zh-CN
---

path: https://gitee.com/kvker/kvker/new/main/source/_posts

```js
(() => {
  const div = document.createElement('textarea')
  div.style.cssText = 'position: fixed; right: 0; top: 50px; width: 300px; height: 200px;background-color: white;'
  const date = new Date()
  , date_label = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
  div.value = `---
layout: note
title: xxx
excerpt: Read more...
date: ${date_label}
updated: ${date_label}
comments: false
lang: zh-CN
---`
  document.body.append(div)
})()
```
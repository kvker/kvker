---
layout: note
title: powershell使用proxy代理
excerpt: Read more...
date: 2022-10-23 22:17:9
updated: 2022-10-23 22:17:9
comments: false
lang: zh-CN
---

```
set http_proxy=http://localhost:8888/
set https_proxy=http://localhost:8888/
```
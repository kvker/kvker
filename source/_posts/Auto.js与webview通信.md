---
layout: note
title: Auto.js与webview通信
excerpt: Read more...
date: 2022-10-6 11:37:33
updated: 2022-10-6 11:37:33
comments: false
lang: zh-CN
---

```js
"ui";

importClass(android.webkit.WebSettings)
ui.layout(
  <vertical id="main">
    <scroll>
      <webview id="webView" />
    </scroll>
  </vertical>
)
var webView = ui.webView
var url = "http://192.168.1.21:8000/webView.html"
/* -------------------------------settings------------------------------------------- */
var settings = webView.getSettings()
settings.setJavaScriptEnabled(true) // 设置是否支持js
settings.setSupportZoom(false)
settings.setAllowFileAccessFromFileURLs(false)
settings.setAllowUniversalAccessFromFileURLs(false)

var WebChromeClient = android.webkit.WebChromeClient
var webChromeClient = new JavaAdapter(WebChromeClient, {
  onConsoleMessage(consoleMessage) {
    let message = consoleMessage.message()
    console.log(message)
  },
  onReceivedTitle(instance, title) {
    console.log(title)
    instance.evaluateJavascript(`;document.querySelector('#p').innerText = '${title}'`, ret => {
      console.log(ret)
    })
  },
})
webView.setWebChromeClient(webChromeClient)

webView.loadUrl(url)
```

```html
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <title>autojs与webview通信</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
  </head>
  <body>
    <h2>autojs与webview通信</h2>
    <div class="container-fluid">
      <div id="dateTime"></div>
    </div>
    <button onclick="clickButton()">click</button>

    <p id="p"></p>

    <script>
      function clickButton() {
        console.log('click button')
        document.title = 'click title'
      }
    </script>
  </body>
</html>
```
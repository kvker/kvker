---
layout: note
title: HTML 标签与原始类型
excerpt: Read more...
date: 2022-7-27 17:49:50
updated: 2022-7-27 17:49:50
comments: false
lang: zh-CN
---

获取原始类型方法

`let element = document.getElementById('div') let type = Object.prototype.toString.call(element).slice(8, -1) console.log(type) // HTMLDivElement`

html (HTMLHtmlElement)

根元素

head (HTMLHeadElement)

title (HTMLTitleElement)

base (HTMLBaseElement)

link (HTMLLinkElement)

meta (HTMLMetaElement)

style (HTMLStyleElement)

script (HTMLScriptElement)

noscript (HTMLElement)

文件数据元素

body (HTMLBodyElement)

section (HTMLElement)

nav (HTMLElement)

article (HTMLElement)

aside (HTMLElement)

h1 h2 h3 h4 h5 h6 (HTMLHeadingElement)

hgroup (HTMLElement)

header (HTMLElement)

footer (HTMLElement)

address (HTMLElement)

文件区域元素

p (HTMLParagraphElement)

hr (HTMLHRElement)

pre (HTMLPreElement)

blockquote (HTMLQuoteElement)

ol (HTMLOListElement)

ul (HTMLUListElement)

li (HTMLLIElement)

dl (HTMLDListElement)

dt (HTMLElement)

dd (HTMLElement)

div (HTMLDivElement)

群组元素

a (HTMLAnchorElement)

em (HTMLElement)

strong (HTMLElement)

small (HTMLElement)

i (HTMLElement)

b (HTMLElement)

span (HTMLSpanElement)

br (HTMLBRElement)

文字层级元素

ins (HTMLModElement)

del (HTMLModElement)

编修记录元素

img (HTMLImageElement)

iframe (HTMLIFrameElement)

embed (HTMLEmbedElement)

object (HTMLObjectElement)

param (HTMLParamElement)

video (HTMLVideoElement)

audio (HTMLAudioElement)

source (HTMLSourceElement)

canvas (HTMLCanvasElement)

内嵌媒体元素

table (HTMLTableElement)

caption (HTMLTableCaptionElement)

tbody (HTMLTableSectionElement)

thead (HTMLTableSectionElement)

tfoot (HTMLTableSectionElement)

tr (HTMLTableRowElement)

td (HTMLTableDataCellElement)

th (HTMLTableHeaderCellElement)

表格元素

form (HTMLFormElement)

fieldset (HTMLFieldSetElement)

legend (HTMLLegendElement)

label (HTMLLabelElement)

input (HTMLInputElement)

button (HTMLButtonElement)

select (HTMLSelectElement)

datalist (HTMLDataListElement)

option (HTMLOptionElement)

textarea (HTMLTextAreaElement)

窗体元素

details (HTMLDetailsElement)

summary (HTMLElement)

command (HTMLCommandElement)

menu (HTMLMenuElement)

交互式元素
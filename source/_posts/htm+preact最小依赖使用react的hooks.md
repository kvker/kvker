---
layout: note
title: htm+preact最小依赖使用react的hooks
excerpt: 太爽了...
date: 2022-9-14 19:1:4
updated: 2022-9-14 19:1:4
comments: false
lang: zh-CN
---

```html
<!DOCTYPE html>
<html lang="zh-CN">
<title>htm preact Demo</title>

<script type="module">
  import { html, useState, render } from '/lib/htm-preact.min.js'

  function App({ page }) {
    const [todos, setTodos] = useState([])
    return html`
      <div class="app">
        <${Header} name="ToDo's (${page})" />
        <ul>
          ${todos.map(todo => html`
            <li key=${todo}>${todo}</li>
          `)}
        </ul>
        <button onClick=${() => setTodos(todos.concat(`Item ${todos.length}`))}>Add Todo</button>
        <${Footer}>footer content here<//>
      </div>
    `
  }

  const Header = ({ name }) => html`<h1>${name} List</h1>`

  const Footer = props => html`<footer ...${props} />`

  render(html`<${App} page="All" />`, document.body);
</script>

</html>
```
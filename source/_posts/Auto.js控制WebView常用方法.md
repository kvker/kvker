---
layout: note
title: Auto.js控制WebView常用方法
excerpt: Read more...
date: 2022-10-6 10:33:45
updated: 2022-10-6 10:33:45
comments: false
lang: zh-CN
---


[牙叔教程](https://www.yuque.com/yashujs/bfug6u/bvga5c)
```js
"ui";
/*
 * @version: 1.0
 * @Date: 2021-12-12 22:45:09
 * @LastEditTime: 2021-12-12 23:36:13
 * @LastEditors: 牙叔
 * @Description:
 * @FilePath: \webview缩放\main.js
 * @名人名言: 牙叔教程 简单易懂
 * @bilibili: 牙叔教程
 * @公众号: 牙叔教程
 * @QQ群: 747748653
 */
importClass(android.webkit.WebSettings);
ui.layout(
  <vertical id="main" padding="10 10 10 30" bg="#ff1e90ff">
    <scroll>
      <webview id="webView" />
    </scroll>
  </vertical>
);
var webView = ui.webView;
var url = "https://www.baidu.com/";
/* -------------------------------settings------------------------------------------- */
var settings = webView.getSettings();
// 电脑头
// settings.setUserAgentString(
//   "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
// );
// 手机头
settings.setUserAgentString("JUC (Linux; U; 2.3.7; zh-cn; MB200; 320*480) UCWEB7.9.3.103/139/999");
// printObj(settings)
settings.setLoadsImagesAutomatically(true); // 是否自动加载图片
settings.setDefaultTextEncodingName("UTF-8"); // 设置默认的文本编码 UTF-8 GBK
settings.setJavaScriptEnabled(true); // 设置是否支持js
settings.setJavaScriptCanOpenWindowsAutomatically(true); // 设置是否允许js自动打开新窗口, window.open
settings.setSupportZoom(true); // 是否支持页面缩放
settings.setBuiltInZoomControls(true); // 是否出现缩放工具
settings.setUseWideViewPort(true); // 容器超过页面大小时, 是否将页面放大到塞满容器宽度的尺寸
settings.setLoadWithOverviewMode(true); // 页面超过容器大小时, 是否将页面缩小到容器能够装下的尺寸
// 自适应屏幕的算法
// public enum LayoutAlgorithm {
//   NORMAL,
//   /**
//    * @deprecated This algorithm is now obsolete.
//    */
//   @Deprecated
//   SINGLE_COLUMN,
//   /**
//    * @deprecated This algorithm is now obsolete.
//    */
//   @Deprecated
//   NARROW_COLUMNS,
//   TEXT_AUTOSIZING
// }
settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING); // 设置自适应屏幕的算法
settings.setAppCacheEnabled(false); // 是否启用app缓存
settings.setAppCachePath("/sdcard/aaa"); // app缓存文件路径
settings.setAllowFileAccess(true); // 是否允许访问文件
settings.setDatabaseEnabled(true); // 是否启用数据库
settings.setDomStorageEnabled(true); // 是否本地存储

/* -------------------------WebViewClient------------------------------------------------- */
var WebViewClient = android.webkit.WebViewClient;
var webViewClient = new JavaAdapter(WebViewClient, {
  onPageStarted: function (view, url, favicon) {
    console.log("onPageStarted");
  },
  onPageFinished: function (view, url) {
    console.log("onPageFinished");
  },
  onReceivedError: function (view, errorCode, description, failingUrl) {
    console.log("onReceivedError");
  },
  shouldOverrideUrlLoading: function (view, url) {
    console.log("shouldOverrideUrlLoading");
  },
  shouldInterceptRequest: function (view, url) {
    console.log("shouldInterceptRequest");
  },
});
webView.setWebViewClient(webViewClient);
/* -------------------------WebChromeClient----------------------------------------------- */
let WebChromeClient = android.webkit.WebChromeClient;
var webChromeClient = new JavaAdapter(WebChromeClient, {
  onReceivedTitle: function (webView, title) {
    console.log("onReceivedTitle");
  },
  onProgressChanged: function (view, progress) {
    console.log("onProgressChanged");
  },
  onJsAlert: function (view, url, message, result) {
    console.log("onJsAlert");
  },
  onJsConfirm: function (view, url, message, result) {
    console.log("onJsConfirm");
  },
  onJsPrompt: function (view, url, message, defaultValue, result) {
    console.log("onJsPrompt");
  },
  onGeolocationPermissionsShowPrompt: function (origin, callback) {
    console.log("onGeolocationPermissionsShowPrompt");
  },
});
webView.setWebChromeClient(webChromeClient);
/* ----------------------------Cookie---------------------------------------------- */
var cookieManager = android.webkit.CookieManager.getInstance();
cookieManager.setAcceptCookie(true);
let ck = cookieManager.getCookie(url);
log("ck = " + ck);
/* -------------------------DownloadListener------------------------------------------------- */
webView.setDownloadListener(
  new android.webkit.DownloadListener({
    onDownloadStart: function (url, userAgent, contentDisposition, mimeType, contentLength) {},
  })
);
/* -------------------------------------------------------------------------- */
webView.post(
  new java.lang.Runnable({
    run: function () {
      toastLog("Runnable");
    },
  })
);
/* -------------------------------------------------------------------------- */
function 网页中获取网页源代码() {
  return document.getElementsByTagName("html")[0].outerHTML;
}
/* -------------------------------------------------------------------------- */
webView.loadUrl(url);
```
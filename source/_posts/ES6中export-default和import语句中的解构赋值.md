---
layout: note
title: ES6中export-default和import语句中的解构赋值
excerpt: 新语法无法在import时候直接解构的原因。
date: 2022-6-14 0:24:54
updated: 2022-6-14 0:24:54
comments: false
lang: zh-CN
---

无法直接在import时候解构的原因：
```js
// 经过 webpack 和 babel 的转换

export default {
  host: 'localhost',
  port: 80
}

// 变成了
module.exports.default = {
  host: 'localhost',
  port: 80
}
```
---
layout: note
title: 单页面history静态文件部署vue的Nginx配置
excerpt: Read more...
date: 2022-10-8 17:33:5
updated: 2022-10-8 17:33:5
comments: false
lang: zh-CN
---

```
server {
    listen 80;
    root /path;
    
    location /any/ {
        add_header Cache-Control no-cache;
        add_header Pragma no-cache;
        try_files $uri $uri/ /any/index.html;
    }
}
```
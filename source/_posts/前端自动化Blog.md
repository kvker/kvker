---
layout: note
title: 前端自动化Blog
excerpt: Read more...
date: 2022-5-30 14:17:11
updated: 2022-5-30 14:17:11
comments: false
lang: zh-CN
---

## 上下文

选择一个相对好看点的Blog框架（样式），用来部署自己的Markdown代码笔记或文章。

自己有一个一直在用的自建的随笔页面，由于是自建，所以功能是非常亲和自己的操作的，比如默认搜索框，PWA等功能。

但是自建的随笔页面有一些痛点，比如样式并不理想（只为实现功能），SEO不好（因为前后端分离），挂个人介绍等地，丑了点……

所以需要找个好用的Blog框架，首先想到的当然是WordPress，但是这玩意儿得装PHP，作为一个“精通”Node.js的大前端，总觉得有点老鼠尾巴落汤里的恶心感，不用JS解决所有问题的不是一个好前端（Doge），于是排除一圈后，最终还是回到了Hexo的怀抱。

最后就是同步存在两个website，一个是自用的快速搜索用，一个是对外的稍微好看点的Blog。

要求：好看（差不多），SEO友好，方便编辑，可自动化部署（核心）。

## 几大知识点

* Hexo，基于Node.js且仍旧积极维护的Blog框架。
* Gitee，码云，国内好用一点的代码托管平台，主要用它的WebHooks。
* Linux服务器或轻应用服务器，用来托管静态文件。
* Node.js简易HTTP服务，用来承接WebHooks接口。
* Node.js执行Linux命令，用来处理收到请求后更新代码与编译。
* Nginx，用来反向代理前端静态文件（或服务）。

## 步骤

先理思路：

1. 我们需要一个基于Node.js的Blog框架并且稳定更新迭代，所以选择Hexo；
2. 如果顺利通过Hexo满足条件，则需要编译出来静态文件，所以选择Hexo的generate模式；
3. 编译出来的文件没有必要push到git上，所以这个编译出来的文件除了本地预览外，只有远端的服务器上需要执行；
4. 基于3，所以需要一个方法，它需要知道文档更新了，并且自行拉取最新的代码，且编译；
5. 基于3、4，推导出使用Gitee（或其他）的WebHooks方案，当代码提交到master分支后，POST一个接口；
6. 我们需要响应这个接口，干三件事：一，跳转到项目文件夹；二，拉取最新代码；三，编译出最新的文档；
7. 基于6，我们需要在服务器上跑一个POST服务，用来响应WebHooks的接口请求，并执行6的步骤；
8. 由于仅仅一个POST服务，所以使用HTTP模块响应，使用child_process来执行终端命令`cd && git && build`。

上面提到的文档地址：

[Hexo](https://hexo.io/zh-cn/index.html)

[Gitee-WebHooks](https://gitee.com/help/categories/40)

[Node.js-HTTP](http://nodejs.cn/api/http.html)

[Node.js-child_process](http://nodejs.cn/api/child_process.html)

---

那么，就可以按照上面的思路，结合文档，快乐的开搞吧。

## 最终效果

* 在gitee上的master分支直接编辑一个新的md文件（可以把编辑页收藏方便后续编辑）；
* WebHooks自动发一个POST请求到服务器；
* 服务器处理完成后，返回一个success的响应；
* 刷新页面看效果。
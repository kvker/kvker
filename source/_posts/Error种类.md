---
layout: note
title: Error种类
excerpt: Read more...
date: 2022-5-29 14:17:11
updated: 2022-5-29 14:17:11
comments: false
lang: zh-CN
---

1. ReferenceError:找不到对象时
2. TypeError:错误的使用了类型或对象的方法时
3. RangeError:使用内置对象的方法时，参数超范围
4. SyntaxError:语法写错了
5. EvalError:错误的使用了Eval   
6. URIError:URI错误
  
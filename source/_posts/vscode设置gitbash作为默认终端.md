---
layout: note
title: vscode设置gitbash作为默认终端
excerpt: 新版的设置方案。
date: 2022-6-13 22:22:23
updated: 2022-6-13 22:22:23
comments: false
lang: zh-CN
---

```json
{
    "terminal.integrated.automationProfile.windows": {
        "GitBash": {
            "path": "C:\\Program Files\\Git\\bin\\bash.exe",
            "args": []
        }
    },
    "terminal.integrated.defaultProfile.windows": "Git Bash"
}
```
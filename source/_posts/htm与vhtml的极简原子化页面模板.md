---
layout: note
title: htm与vhtml的极简原子化页面模板
excerpt: Read more...
date: 2022-10-9 15:13:11
updated: 2022-10-9 15:13:11
comments: false
lang: zh-CN
---

index.html
```html
<!DOCTYPE html>
<html lang="zh-CN">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Origin Demo</title>
  <style>
    .container {
      display: contents;
    }
  </style>
</head>

<body>
  <script src="https://unpkg.com/htm@3.1.1/mini/index.js"></script>
  <script src="https://unpkg.com/vhtml@2.2.0/dist/vhtml.min.js"></script>

  <script>
    const html = htm.bind(vhtml)

    fetch('config.json')
      .then(ret => {
        if (ret.ok) {
          return ret.json()
        }
      })
      .then(json => {
        if (json.code) return
        let { list, card } = json.data

        if (list) {
          let hs = html`${list.reduce((p, c) => p += `<div class="item">${c}</div>`, '')}`
          let dom = document.createElement('div')
          dom.classList = 'list'
          dom.innerHTML = hs
          document.body.insertAdjacentElement('beforeend', dom)
        }

        if (card) {
          let { title, time, location } = card
          document.body.insertAdjacentHTML('beforeend', html`<section class="card-box">
            <h3 class="card-title">${title}</h3>
            <div class="card-body">
              <p class="time">${time}</p>
              <p class="location">${location}</p>
            </div>
          </section>`)
        }
      })
  </script>
</body>

</html>
```

config.json
```json
{
  "code": 0,
  "message": "success",
  "data": {
    "list": ["1", "2"],
    "card": {
      "title": "im title",
      "time": "2022-10-09",
      "location": "HangZhou"
    }
  }
}
```
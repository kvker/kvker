---
layout: note
title: python获取当前执行路径
excerpt: Read more...
date: 2023-6-7 18:14:30
updated: 2023-6-7 18:14:30
comments: false
lang: zh-CN
---

```
// 前者是exe，后者是.py文件
abspath = os.path.dirname(sys.executable) if getattr(sys, 'frozen', False) else os.path.dirname(os.path.abspath(__file__))
```
---
layout: note
title: canvas绘制图片跨域问题处理
excerpt: Read more...
date: 2022-12-4 13:54:46
updated: 2022-12-4 13:54:46
comments: false
lang: zh-CN
---

类似如下错误：
`Uncaught DOMException: Failed to execute 'toDataURL' on 'HTMLCanvasElement': Tainted canvases may not be exported`

1. 服务端开启`Access-Control-Allow-Origin: [*|域名]`
2. img标签或对象，添加crossorigin="anonymous"，解决不要求图片携带用户验证信息，如cookie等

备注，crossorigin有两个值，默认是use-credentials，即带用户验证信息，""空字符也为anonymous
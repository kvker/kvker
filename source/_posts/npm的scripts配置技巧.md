---
layout: note
title: npm的scripts配置技巧
excerpt: post与pre
date: 2022-8-22 16:11:3
updated: 2022-8-22 16:11:3
comments: false
lang: zh-CN
---

[post与pre](https://docs.npmjs.com/cli/v6/using-npm/scripts)

`npm run command` 这里的 command 可以分别添加一个 precommand 和 postcommand，分别在 command 本身执行的前面和后面执行，如下参考：

```
"predo": "echo 0",
"do": "echo 1",
"postdo": "echo 2"
```

执行 `npm run do` 会顺序执行 predo do postdo 并在控制台打印 0 1 2。
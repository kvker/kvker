---
layout: note
title: 3D翻转实例
excerpt: Read more...
date: 2023-5-16 20:32:7
updated: 2023-5-16 20:32:7
comments: false
lang: zh-CN
---

```html
<div class="letter-top-box">
        <img class="letter-top" src="img/letter-top.png" alt="letter-top" />
        <img class="letter-top-cover" src="img/letter-top-cover.png" alt="letter-top" />
      </div>
```

```css
.letter-top-box {
        position: absolute;
        top: 6px;
        left: 0;
        width: 100%;
        height: 50%;
        transform-origin: 0 0;
        transform-style: preserve-3d;
        transition: 1.2s linear transform;
        z-index: 4;
      }

.letter-top-box .letter-top-cover,
      .letter-top-box .letter-top {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 160px;
        backface-visibility: hidden;
        transform-style: preserve-3d;
      }
```


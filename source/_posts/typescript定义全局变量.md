---
layout: note
title: typescript定义全局变量
excerpt: Read more...
date: 2022-10-17 15:20:20
updated: 2022-10-17 15:20:20
comments: false
lang: zh-CN
---

1. 在项目根目录创建typings目录
2. 在typings中添加index.d.ts，在其中定义你要的类型
interface MyType {
  foo: string;
  bar: string[];
}

3. 修改tsconfig.json，添加如下行
```json{
  "compilerOptions": {
    "typeRoots": ["./node_modules/@types/", "./typings"],
  }
}
```

4. 重新编编译ts
---
layout: note
title: webhooks实例
excerpt: Read more...
date: 2022-5-29 16:17:11
updated: 2022-5-29 16:17:11
comments: false
lang: zh-CN
---

基于hexo的实例

```js
const http = require('http')
  , exec = require('child_process').exec

const server = http.createServer(function (req, res) {
  if(req.url === "/") {
    res.writeHead(200, { 'Content-type': 'text/html' });
    res.write('<h1>Node.js</h1>');
    res.end('<p>Hello World</p>');
  }
  if(req.url === '/update/person') {
    exec('cd ~/projects/cat/person/ && hexo g', (error, stdout, stderr) => {
      console.log({ error, stdout, stderr })
      if(error) {
        res.end(error.message)
      } else res.end(JSON.stringify({
        code: 200,
        message: 'update cat\/person success',
      }))
    })
  }
})

server.listen('13098', function () {
  console.log((new Date()) + ' Server is listening on port 13098', 13098);
})

```
---
layout: note
title: form阻止提交禁止提交
excerpt: Read more...
date: 2023-11-23 11:50:35
updated: 2023-11-23 11:50:35
comments: false
lang: zh-CN
---

如果要借用 form 的 require 能力，这时候的 submit 不要直接在 form 上面使用 onsubmit，使用 `form.onsubmit = function` 来处理。

```js
let form = qeury
form.onsubmit = function(e) {
    e.preventDefault()
}
```
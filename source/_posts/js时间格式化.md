---
layout: note
title: js时间格式化.
excerpt: Read more...
date: 2023-3-12 22:3:1
updated: 2023-3-12 22:3:1
comments: false
lang: zh-CN
---

```js
formatDate(date) {
    const year = date.getFullYear()
    const month = ('0' + (date.getMonth() + 1)).slice(-2)
    const day = ('0' + date.getDate()).slice(-2)
    const hours = ('0' + date.getHours()).slice(-2)
    const minutes = ('0' + date.getMinutes()).slice(-2)
    const seconds = ('0' + date.getSeconds()).slice(-2)
    return `${year}/${month}/${day} ${hours}:${minutes}:${seconds}`
  }
```
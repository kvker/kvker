---
layout: note
title: nodejs内置cropt模块生成uuid
excerpt: Read more...
date: 2023-2-22 18:19:9
updated: 2023-2-22 18:19:9
comments: false
lang: zh-CN
---

```js
const crypto = require("crypto")
const uuid = crypto.randomUUID()
```
---
layout: note
title: Ubuntu服务器安装图形界面GUI
excerpt: 一步步操作
date: 2022-5-29 14:17:11
updated: 2022-5-29 14:17:11
comments: false
lang: zh-CN
---

# 安装桌面
`apt install ubuntu-gnome-desktop`
`systemctl set-default multi-user.target`
`startx`

# 安装VNC
`apt install tigervnc-standalone-server`

# 配置VNC服务
`sudo vncpasswd`
在`~/.vnc/`下面新建xstartup文件，这个是vncserver启动时候运行的命令，参考输入如下：
```shell

#!/bin/sh
# Start Gnome 3 Desktop 
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
vncconfig -iconic &
dbus-launch --exit-with-session gnome-session &
```

# 启动VNC服务
`vncserver`
`vncserver -localhost no`此句是为了避免只监听本地`127.0.0.1`从而导致无法外网连接。

# 连接VNC
使用VNCViewer之类的连接，记得服务器防火墙要开启5900－5999之间的端口号，避免无法连接。

# 其他

## 查看VNC会话
`vncserver -list`

## 关闭VNC会话
`vncserver -kill :display-id`
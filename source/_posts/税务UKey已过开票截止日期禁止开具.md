---
layout: note
title: 税务UKey已过开票截止日期禁止开具
excerpt: 解决方案
date: 2022-9-10 16:53:2
updated: 2022-9-10 16:53:2
comments: false
lang: zh-CN
---

汇总上传与反写监控在同一个页面，位于数据管理-汇总上传。

汇总上传，反写监控，多次后达到过期时间超过当前即可。
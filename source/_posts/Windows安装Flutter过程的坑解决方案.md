---
layout: note
title: Windows安装Flutter过程的坑解决方案
excerpt: Read more...
date: 2023-3-10 10:18:34
updated: 2023-3-10 10:18:34
comments: false
lang: zh-CN
---

1. 环境变量，Windows的环境变量老生常谈，需要设置的环境变量涉及：ANDROID_HOME ANDROID_SDK_ROOT JAVA_HOME ADB ANDROID_CMDLINE_TOOLS Flutter GRADLE CHROME_EXECUTABLE等
2. Visual Studio相关工具安装，需要的有：C++ Make，Windows10 SDK，MCKV
3. PowerShell下的代理设置

```powershell
function proxy {
  $env:HTTPS_PROXY="http://127.0.0.1:7078"
  $env:HTTPS_PROXY="http://127.0.0.1:7078"
  $env:NO_PROXY="localhost,127.0.0.0,gitee.com"
}
```

参考上面设置自己的代码，然后启动命令行后，如有必要执行`proxy`
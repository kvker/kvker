---
layout: note
title: hUniAPP与H5通信bridge
excerpt: Read more...
date: 2022-9-5 23:31:54
updated: 2022-11-29 23:31:54
comments: false
lang: zh-CN
---

# APP端

bridge.js
```js
import callback_pool from './callback_pool.js'

export default new class Bridge {
  constructor() {
    this.callback_pool = callback_pool
  }

  routeToSync(url) {
    let [host, query] = url.split('?')
    if (query && query.includes('login=') && !uni.getStorageSync('userinfo')) {
      uni.showToast({
        title: '未登录',
        icon: 'none'
      })
      uni.navigateTo({
        url: '/pages/account/login/login'
      })
    } else {
      uni.navigateTo({
        url: `/pages/bridge/bridge?url=${url}`
      })
    }
  }

  /**
   * 注册事件
   * @param {object} data {params: object, key: string}
   */
  regist(data) {
    const {
      params,
      key
    } = data
    return this.checkKey(key)
  }

  /**
   * 回调，若没有key则会一次性执行所有的回调
   */
  callback(webview, key) {
    return this.checkKey(key)
      .then(() => this.callback_pool[key]())
      .then(params => this.runJS(webview, `bridge.reciveHandler('${key}', ${JSON.stringify(params)})`))
      .then((ret) => console.log('runJS结束，返回值为: ' + ret))
  }

  /**
   * @param {string} js 运行网页js，若有回调则继续执行回调
   */
  runJS(webview, js) {
    return new Promise((resolve, reject) => {
      if (typeof(js) === 'string' && js.trim().length) {
        resolve(webview.evalJS(js))
      } else {
        console.error({
          js
        })
        reject(new Error('脚本异常'))
      }
    })
  }

  // 工具
  checkKey(key) {
    return new Promise((resolve, reject) => {
      if (this.callback_pool[key]) resolve(true)
      else reject(this.errorSync('非法key'))
    })
  }

  errorSync(message) {
    return new Error(message)
  }
}
```

callback_pool.js
```js
export default {
  // 默认均为异步
  USERINFO: () => Promise.resolve(uni.getStorageSync('userinfo')),
  OPEN_PAGE: (url) => Promise.resolve(uni.navigateTo({
    url
  })),
  OPEN_WEBVIEW_PAGE: (url) => Promise.resolve(this.routeToSync(url)),
  SYSTEM_INFO: () => Promise.resolve(uni.getSystemInfoSync()),
  DEVICE: () => Promise.resolve(plus.device),
  // 同步方法
  CLOSE_CURRENT_PAGE_SYNC: () => uni.navigateBack(),
  RELAUNCH_SYNC: () => uni.reLaunch(),
  BACK_PAGE_SYNC: (delta) => uni.navigateBack({
    delta
  }),
}
```

# H5端

bridge.js
```js
export default new class Bridge {
  constructor() {
    console.log('bridge init success')
    this.adapter()
    this.callback_pool = {}
  }

  adapter() {
    if (process.client) {
      document.addEventListener('UniAppJSBridgeReady', () => {
        uni.getEnv(({ h5, plus }) => {
          let event = new Event('UniLoaded')
          event.plus = plus
          event.h5 = h5
          this.instance = uni
          if (plus) {
            bridge.postMessage({
              key: 'USERINFO',
              callback: userinfo => {
                localStorage.setItem('userinfo', JSON.stringify(userinfo))
                bridge.postMessage({
                  key: 'DEVICE',
                  callback: device => {
                    localStorage.setItem('device', JSON.stringify(device))
                    document.dispatchEvent(event)
                  }
                })
              }
            })
          } else {
            document.dispatchEvent(event)
          }
        })
      })
    }
  }

  /**
   * 传参事件
   * @param {object} data {params: object, key: string, callback: function}
   */
  postMessage(data) {
    let { key, callback } = data
    if (!this.instance) this.instance = uni // 这行主要是调试用，必要注释，不影响
    return new Promise((resolve, reject) => {
      if (key) {
        console.log('bridge key: ' + key)
        this.callback_pool[key] = callback // 注册回调
        this.instance.postMessage({ data })
        resolve(true)
      } else {
        reject('请写入key')
      }
    })
  }

  /**
   * 接收处理，提供给移动端调用
   * @param {string} key 事件key
   * @param {object} params 返回的参数
   */
  reciveHandler(key, params) {
    // console.log(key, params)
    return new Promise((resolve, reject) => {
      if (key) {
        let callback = this.callback_pool[key]
        callback && callback(params)
        resolve(true)
      } else {
        reject('回调无key')
      }
    })
  }
}
```

mixin
```js
import Vue from 'vue'

Vue.mixin({
  mounted() {
    // 需要的页面自行实现installBridge
    if (this.installBridge) document.addEventListener('UniLoaded', this.installBridge.bind(this))
  }
})
```

调用
```js
export default {
  methods: {
    installBridge() {
      this.getApplyList()
    }
  }
}
```
---
layout: note
title: echarts的服务化
excerpt: Read more...
date: 2022-8-29 17:55:10
updated: 2022-8-29 17:55:10
comments: false
lang: zh-CN
---

```js
import * as echarts from 'echarts'

export default new class Charts {
  constructor() {
    console.log('charts init success')
    this.instance = {}
  }

  regist(id, dom, options) {
    let charts = echarts.init(dom)
    charts.setOption(options)
    this.instance[id] = charts
    return this.instance[id]
  }

  update(id, callback) {
    let charts = this.instance[id]
    callback && typeof(callback) === 'function' && charts.setOption(callback(charts.getOption()))
  }
}
```
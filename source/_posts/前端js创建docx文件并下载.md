---
layout: note
title: 前端js创建docx文件并下载
excerpt: Read more...
date: 2023-10-7 21:15:15
updated: 2023-10-7 21:15:15
comments: false
lang: zh-CN
---

依赖docx.js与filesaver.js
```js
let createSection = ps => {
        return {
          children: ps
        }
      }
      let createParagraph = (text, params = {}) => {
        return new docx.Paragraph({
          children: [createTextRun(text)],
          ...params,
        })
      }
      let createParagraphs = (text, params = {}) => {
        return text.split('\n').map(t => createParagraph(t, params))
      }
      let createTitle = (text, level = docx.HeadingLevel.HEADING_1) => {
        return createParagraph(text, {
          heading: level,
          alignment: docx.AlignmentType.CENTER,
        })
      }
      let createEmptyLine = () => {
        return createTitle('')
      }
      let createTextRun = (text, params = {}) => {
        return new docx.TextRun({
          text,
          font: '宋体',
          size: 24,
          ...params
        })
      }

      let section = createSection([createTitle('摘要'), createEmptyLine(), ...createParagraphs(input.abstract), createEmptyLine(), ...createParagraphs('关键词：' + input.key_words)])
      sections.push(section)

      for(const item of output) {
        if(item.subs && item.subs.length) {
          for(const sub of item.subs) {
            if(sub.content) {
              section = createSection([createTitle(sub.title, docx.HeadingLevel.HEADING_2), createEmptyLine(), ...createParagraphs(sub.content)])
              sections.push(section)
            }
          }
        } else if(item.content) {
          section = createSection([createTitle(item.title), createEmptyLine(), ...createParagraphs(item.content)])
          sections.push(section)
        } else {
          section = createSection([createTitle('生成失败'), createEmptyLine(), ...createParagraphs('本章节生成失败')])
          sections.push(section)
        }
      }
      console.log(sections)
      const doc = new docx.Document({
        sections
      })

      docx.Packer.toBlob(doc).then((blob) => {
        // console.log(blob)
        saveAs(blob, input.title + ".docx")
        console.log("Document created successfully")
      })
    ```
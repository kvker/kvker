---
layout: note
title: css斜切角实现
excerpt: Read more...
date: 2023-7-25 9:38:11
updated: 2023-7-25 9:38:11
comments: false
lang: zh-CN
---

从左上角右边那个算第一个开始，一直配置到左上角左边结束，一共八个点。
```css
.result-img {
  width: 100%;
  height: 100%;
  clip-path: polygon(12px 0, 100% 0,
    100% 0, 100% calc(100% - 4px),
    calc(100% - 4px) 100%, 24px 100%,
    0 calc(100% - 20px), 0 12px);
}
```
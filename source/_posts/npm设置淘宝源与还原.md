---
layout: note
title: npm设置淘宝源与还原
excerpt: Read more...
date: 2022-11-17 22:2:49
updated: 2022-11-17 22:2:49
comments: false
lang: zh-CN
---

```shell
// npm设置新淘宝源
npm config set registry https://registry.npmmirror.com
// npm设置回本源
npm config set registry https://registry.npmjs.org
```
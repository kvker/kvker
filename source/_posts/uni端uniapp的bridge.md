---
layout: note
title: uni端uniapp的bridge
excerpt: Read more...
date: 2022-9-5 23:30:54
updated: 2022-9-5 23:30:54
comments: false
lang: zh-CN
---

```js
export default new class Bridge {
  constructor() {
    console.log('bridge init success')
    this.callback_pool = {
      USERINFO: () => Promise.resolve(uni.getStorageSync('userinfo')),
    }
  }

  routeTo(url) {
    let [host, query] = url.split('?')
    if (query && query.includes('login=') && !uni.getStorageSync('userinfo')) {
      uni.showToast({
        title: '需要登录',
        icon: 'error'
      })
      setTimeout(() => {
        uni.navigateTo({
          url: '/pages/account/login/login'
        })
      }, 1500)
    } else {
      uni.navigateTo({
        url: `/pages/bridge/bridge?url=${url}`
      })
    }
  }

  /**
   * 注册事件
   * @param {object} data {params: object, key: string}
   */
  regist(data) {
    const {
      params,
      key
    } = data
    return this.checkKey(key)
  }

  /**
   * 回调，若没有key则会一次性执行所有的回调
   */
  callback(webview, key, params = {}) {
    return this.checkKey(key)
      .then(() => this.callback_pool[key]()).then(params => webview.evalJS(
        `bridge.recivesHandler('${key}', ${JSON.stringify(params)})`))
  }

  /**
   * @param {string} js 运行网页js，若有回调则继续执行回调
   */
  runJS(webview, js) {
    return new Promise((resolve, reject) => {
      if (typeof(js) === 'string' && js.trim().length) {
        resolve(webview.evalJS(js))
      } else reject(new Error('脚本异常'))
    })
  }

  // 工具
  checkKey(key) {
    return new Promise((resolve, reject) => {
      if (this.callback_pool[key]) resolve(true)
      else reject(this.errorSync('非法key'))
    })
  }

  errorSync(message) {
    return new Error(message)
  }
}
```
---
layout: note
title: Windows必装软件
excerpt: Read more...
date: 2022-7-5 17:22:4
updated: 2022-7-5 17:22:4
comments: false
lang: zh-CN
---

* ImTip，自动标记当前输入法中英文状态
* uTools，alt+空格神器
* Everything，高速系统搜索
* QuickLook，空格快速预览
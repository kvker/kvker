---
layout: note
title: display的contents说明
excerpt: Read more...
date: 2022-8-25 17:16:27
updated: 2022-8-25 17:16:27
comments: false
lang: zh-CN
---

`display: contents;`

可以理解为 wrapper 来用，提供了 DOM 结构树，同时又不会收到布局影响，仅作为不占用空间的容器使用。
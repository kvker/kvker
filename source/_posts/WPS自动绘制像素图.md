---
layout: note
title: WPS自动绘制像素图.
excerpt: 很有用
date: 2022-7-14 14:49:22
updated: 2022-7-14 14:49:22
comments: false
lang: zh-CN
---

**色调一定要尽可能少，不然容易爆WPS单元格样式限制，越少越好！**

1. 分解图片为BGR数值的二维数组化的CSV
2. 将二维数组的CSV导入数据，数据-导入-选择数据源-分隔符号-逗号-完成
3. 插入脚本，开发工具-WPS宏编辑器-复制粘贴下面脚本，根据1步骤获取的图片宽高修改（修改img_width与img_height）
3. 运行脚本自动填充颜色

[图片分析获取数据源](https://codepen.io/zweizhao/pen/jOzVWzg)

```html
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>获取图片像素矩阵</title>
  </head>
  <body>
    <canvas id="canvas_id"></canvas>
    <div id="box"></div>
    <script>
      // 10进制转16进制，统一返回两位
      function fixNum(num) {
        let c = num.toString(16)
        if (c.length === 1) {
          c = '0' + c
        }
        return c
      }
      // 获取canvas
      const canvas = document.getElementById('canvas_id'),
        ctx = canvas.getContext('2d'),
        scale = 1,
        // 创建图片对象
        img = new Image()
      // 图片地址自己更换一下
      img.src = 'test.png'
      // 图片的比例数值
      // 图片加载完毕回调函数
      img.onload = function () {
        // 图片宽高
        const w = Math.floor(img.width * scale),
          h = Math.floor(img.height * scale)
        // 画布宽高
        canvas.width = w
        canvas.height = h
        // 绘制图片到canvas
        ctx.drawImage(img, 0, 0, w, h)
        // 获取图片数据
        const imgData = ctx.getImageData(0, 0, w, h),
          // 存储颜色列表
          list = []
        let sub_list = [],
          result = ''
        let h_index = 0
        for (let i = 0; i < imgData.data.length; i += 4) {
          if (~~(i / (w * 4)) !== h_index) {
            h_index++
            console.log(h_index)
            result += `\n`
          }
          result +=
            imgData.data[i + 2] * 256 * 256 +
            imgData.data[i + 1] * 256 +
            imgData.data[i] +
            ','
        }

        // 把图宽放到文本框
        let e = document.createElement('textarea')
        e.value = img.width
        box.append(e)
        // 把像素数组放到文本框
        e = document.createElement('textarea')
        e.value = result //JSON.stringify(list)
        box.append(e)
      }
    </script>
  </body>
</html>
```

自动化宏脚本
```js
function 填色()
{
	const img_width = 676 // 最大宽支持
	, img_height = 200
	, a2z = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
	, a2z_length = a2z.length
	, column_list = []
	, column_width = 2
	let interior
	// 处理列宽
	for(let i = 0; i < img_width; i++) {
		const times = ~~(i / a2z.length) // 第几次循环，先限制在26*26宽度次上，再多则递归处理(不建议，容易挂掉)
		, current_index = i % a2z_length
		, column_name = times ? a2z[times - 1] + a2z[current_index] : a2z[current_index]
		, item = Columns.Item(`${column_name}:${column_name}`)

		item.EntireColumn.Hidden = false
		item.ColumnWidth = column_width
		
		column_list.push(column_name)
	}

	// 填充颜色
	const column_list_length = column_list.length
	for(let i = 0; i < img_width * img_height; i++) {
		const current_line = ~~(i / column_list_length) + 1
		, item_name = column_list[i % column_list_length] + current_line
		, range = Range(item_name)
		
		range.Select()
		interior = Selection.Interior
		interior.Pattern = xlPatternSolid
		interior.Color = range.Value2
		range.Value2 = ''
		interior.TintAndShade = 0
		interior.PatternColorIndex = -4105
	}
}
```
---
layout: note
title: tsconfig配置
excerpt: Read more...
date: 2022-9-13 15:26:11
updated: 2022-9-13 15:26:11
comments: false
lang: zh-CN
---

## 用户settings.json的配置

```json
{
    // 重新设定tabsize
    "editor.tabSize": 2,
    "prettier.tabWidth": 2, // 缩进字节数
    // #每次保存的时候自动格式化 
    "editor.formatOnSave": true,
    // #每次保存的时候将代码按eslint格式进行修复 ,"eslint.autoFixOnSave": true 这个已经过时了
    "editor.codeActionsOnSave": {
        "source.fixAll": true
    },
    // 添加 vue,ts 支持，官方是不推荐用这个，但是你为了是ts文件在vscode自动提示而不是文件编译才提示就必须加这个
    "eslint.validate": [
        "javascript",
        "javascriptreact",
        "typescript",
        {
            "language": "vue",
            "autoFix": true
        }
    ],
    //  #默认是true加上分号，false是在有些容易出问题的地方（ASI failures）首部加分号
    //  详细请看https://prettier.io/docs/en/rationale.html#semicolons
    "prettier.semi": true,
    //  #使用单引号替代双引号,不生效就是eslint做了限制
    "prettier.singleQuote": true,
    //  #让函数(名)和后面的括号之间加个空格
    "javascript.format.insertSpaceBeforeFunctionParenthesis": true,
    "javascript.format.enable": false,
    // #这个按用户自身习惯选择 
    "vetur.format.defaultFormatter.html": "js-beautify-html",
    // #让vue中的js按编辑器自带的ts格式进行格式化 
    // 如果是ts就使用prettier-eslint ，这个需要cpm
    // 这里提示ts没有eslint这个值。但是实测是生效的
    "vetur.format.defaultFormatter.ts": "prettier-eslint",
    "vetur.format.defaultFormatter.js": "prettier-eslint",
    "vetur.format.defaultFormatterOptions": {
        "js-beautify-html": {
            "wrap_attributes": "force-expand-multiline",
            "end_with_newline": false
            // #vue组件中html代码格式化样式
        }
    },
    "editor.fontSize": 16,
    "terminal.integrated.rendererType": "dom",
    "window.zoomLevel": 0,
    "vscode_vibrancy.opacity": -1,
    "vscode_vibrancy.theme": "Default Dark",
    "glassit.alpha": 220,
    "vscode_vibrancy.type": "acrylic",
    "search.followSymlinks": false,
    "[vue]": {
        "editor.defaultFormatter": "octref.vetur"
    },
    "editor.detectIndentation": false,
    "vetur.format.options.tabSize": 4,
}
```

## 单双引号无法切换和结尾不加分号，在当前项目目录下新建.prettierrc.json文件

```json
{
   "singleQuote": true,
   "semi": false
}
```

## tslint.json的配置

```json
{
  "defaultSeverity": "warning",
  "extends": [
    "tslint:recommended"
  ],
  "linterOptions": {
    "exclude": [
      "node_modules/**"
    ]
  },
  "rules": {
    "prefer-const": false,
    "member-ordering": false,
    "indent": [
      true,
      "spaces",
      2
    ],
    "interface-name": false,
    "no-consecutive-blank-lines": false,
    "object-literal-sort-keys": false,
    "ordered-imports": false,
    "quotemark": [
      true,
      "single"
    ],
    "semicolon": [
      false,
      "always"
    ],
    "trailing-comma": [
      true,
      { //对尾随逗号的校验
        "multiline": {
          "objects": "ignore",
          "arrays": "never",
          "functions": "never",
          "typeLiterals": "ignore"
        },
        "esSpecCompliant": true //是否允许尾随逗号出现在剩余变量中
      }
    ]
  }
}
```
---
layout: note
title: Nginx的root与alias在location中的区别
excerpt: Read more...
date: 2022-10-10 17:11:24
updated: 2022-10-10 17:11:24
comments: false
lang: zh-CN
---

nginx指定文件路径有两种方式root和alias，root与alias主要区别在于nginx如何解释location后面的uri，这会使两者分别以不同的方式将请求映射到服务器文件上。

最基本的区别

alias 指定的目录是准确的，给location指定一个目录。

root 指定目录的上级目录，并且该上级目录要含有locatoin指定名称的同名目录。

以root方式设置资源路径：

语法: root path;

配置块: http、server、location、if

以alias 方式设置资源路径

语法: alias path;

配置块: location

Example:

```
location /img/ {
	alias /var/www/image/;
}
```
若按照上述配置的话，则访问/img/目录里面的文件时，ningx会自动去/var/www/image/目录找文件
```
location /img/ {
	root /var/www/image;
}
```
若按照这种配置的话，则访问/img/目录下的文件时，nginx会去/var/www/image/img/目录下找文件
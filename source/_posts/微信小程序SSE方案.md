---
layout: note
title: 微信小程序SSE方案
excerpt: Read more...
date: 2023-9-20 22:15:32
updated: 2023-9-20 22:15:32
comments: false
lang: zh-CN
---

小程序端, UniAPP代码
```js
Vue.prototype.$sse = async function(params, cb) {
  // 获取AI信息, 小程序只有wx支持chunked
  const requestTask = wx.request({
    url: 'sse_api_url',
    method: 'POST',
    headers: {
      "Content-Type": "application/json;charset=utf-8",
      "Accept": 'text/event-stream',
      "Transfer-Encoding": 'chunked',
    },
    timeout: 50000,
    responseType: 'text',
    enableChunked: true,
    data: {
      need_encode: true,
      ...params,
    },
  })

  requestTask.onChunkReceived((response) => {
    const arrayBuffer = response.data
    const uint8Array = new Uint8Array(arrayBuffer)
    let chunk = String.fromCharCode.apply(null, uint8Array)
    chunk = decodeURIComponent(chunk)
    cb(chunk)
  })
}
```

服务端在原本支持text/event-stream情况下，headers再加'Transfer-Encoding': 'chunked'即可。推荐库，axios。
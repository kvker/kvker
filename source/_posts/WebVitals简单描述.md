---
layout: note
title: Web Vitals
excerpt: Read more...
date: 2022-7-12 15:54:30
updated: 2022-7-12 15:54:30
comments: false
lang: zh-CN
---

大体来说：

Largest Contentful Paint (LCP) ：最大内容绘制，测量加载性能。为了提供良好的用户体验，LCP 应在页面首次开始加载后的2.5 秒内发生。

First Input Delay (FID) ：首次输入延迟，测量交互性。为了提供良好的用户体验，页面的 FID 应为100 毫秒或更短。

Cumulative Layout Shift (CLS) ：累积布局偏移，测量视觉稳定性。为了提供良好的用户体验，页面的 CLS 应保持在 0.1. 或更少。

为了确保您能够在大部分用户的访问期间达成建议目标值，对于上述每项指标，一个良好的测量阈值为页面加载的第 75 个百分位数，且该阈值同时适用于移动和桌面设备。

如果一个页面满足上述全部三项指标建议目标值的第 75 个百分位数，那么评估核心 Web 指标合规性的工具应评判该页面为通过。
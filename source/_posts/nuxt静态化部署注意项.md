---
layout: note
title: nuxt静态化部署注意项
excerpt: Read more...
date: 2022-8-29 14:55:45
updated: 2022-8-29 14:55:45
comments: false
lang: zh-CN
---

nuxt.config.js
```
target: 'static',
router: {
  base: '/soldier-demo/'
},
```

html,css静态资源引用换成`~@/static/xxx/xxx`
---
layout: note
title: 微信读书平板PC竖屏优化油猴
excerpt: Read more...
date: 2023-11-21 15:6:58
updated: 2023-11-21 15:6:58
comments: false
lang: zh-CN
---

```js
(() => {
	document.querySelectorAll('canvas').forEach(i => i.style.width = '100%')
 	document.querySelector('.app_content').style.maxWidth  = '100%'
	document.querySelector('.readerTopBar').style.maxWidth  = '100%'
	document.querySelectorAll('.reader_pdf_page_canvasWrapper').forEach(i => i.style.width = '100%')
	window.dispatchEvent(new Event('resize'))
})()

```
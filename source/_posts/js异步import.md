---
layout: note
title: js异步import
excerpt: Read more...
date: 2022-5-31 23:17:11
updated: 2022-5-31 23:17:11
comments: false
lang: zh-CN
---

`import` 运算符，由于需要接 `.then`，所以使用 `import(path)`

`export default` 异步加载为 `ret.default.xxx`，直接 `export` 异步加载为 `ret.xxx`

```js
export default new class {
  constructor() {
    this._av = null
    this._device = null
    this._user = null
    this._util = null
    this._ui = null
  }

  av() {
    return new Promise(s => {
      if (this._av) s(this._av)
      else import('./av/av.js').then(ret => s(ret.default))
    })
  }

  device() {
    return new Promise(s => {
      if (this._device) s(this._device)
      else import('./device/device.js').then(ret => s(ret.default))
    })
  }
  
  user() {
    return new Promise(s => {
      if (this._user) s(this._user)
      else import('./user/user.js').then(ret => s(ret.default))
    })
  }
  
  util() {
    return new Promise(s => {
      if (this._util) s(this._util)
      else import('./util/util.js').then(ret => s(ret.default))
    })
  }
  
  ui() {
    return new Promise(s => {
      if (this._ui) s(this._ui)
      else import('./ui/ui.js').then(ret => s(ret.default))
    })
  }
}
```
---
layout: note
title: rollup最简单压缩代码
excerpt: Read more...
date: 2022-10-9 16:13:3
updated: 2022-10-9 16:13:3
comments: false
lang: zh-CN
---

rollup.config.js
```js
import { uglify } from 'rollup-plugin-uglify'

export default {
  input: 'src/index.js',
  output: [
    {
      file: 'dist/index.cjs.js',
      format: 'cjs',
      name: 'index'
    },
    {
      file: 'dist/index.iife.js',
      format: 'iife',
      name: 'index'
    },
    {
      file: 'dist/index.umd.js',
      format: 'umd',
      name: 'index'
    },
    {
      file: 'dist/index.amd.js',
      format: 'amd',
      name: 'index'
    },
    {
      file: 'dist/index.esm.js',
      format: 'esm',
      name: 'index'
    }
  ],
  plugins: [
    uglify()
  ]
}
```
const exec = require('child_process').exec
const http = require('http')

http.createServer(function(req, res){
  console.log(req.url)
  console.log(req.method)
  if(req.method === 'POST' && req.url === '/update/person') {
    exec('npm run pm', (error, data) => {
      console.log(data)
      res.writeHead(200)
      res.end(JSON.stringify({code: 200, msg: data}))
    })
  } else {
    res.writeHead(404)
    res.end(JSON.stringify({code: 0, msg: '404'}))
  }
}).listen(13098)
